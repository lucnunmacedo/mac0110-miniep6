# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
 	k=n*n*n
	i=1
	j=1
	l=1
	m=1
	while k!=j
		j=0
		l=i
		for a=1:m
			
			j=j+i
			i=i+2
			
		end
		m+=1		
	end
	return l
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
 	k=m*m*m
	i=1
	j=1
	l=1
	n=1
	while k!=j
		j=0
		l=i
		for a=1:n
			
			j=j+i
			i=i+2
			
		end
		n+=1		
	end
	print(m," ",j," ",l)
		for a=2:n-1
			print(" ",(l+2*(a-1)))
		end
		println()
end

function mostra_n(n)
	for a=1:n
		imprime_impares_consecutivos(a)
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()